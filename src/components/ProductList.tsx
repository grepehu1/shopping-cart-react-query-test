import { useQuery } from "@tanstack/react-query"
import { fetchProducts } from "../api/get-products"
import { useCartContext } from "../contexts/cart"
import { Product } from "../utils/types"
import { formatPrice } from "../utils/utils"

export default function ProductList() {
    const { isLoading, data } = useQuery({
        queryKey: ['products'],
        queryFn: fetchProducts,
    })

    if (isLoading) {
        return <div>Loading...</div>
    }

    if (!data) return <div>Error!</div>

    return (
        <ul className="flex flex-wrap p-2 gap-2 w-2/3">
            {data?.map(product => {
                return (
                    <ProductItem key={product.id} product={product} />
                )
            })}
        </ul>
    )
}

function ProductItem({
    product
}: {
    product: Product
}) {
    const { products, addProduct, removeProduct } = useCartContext()

    const isIn = products.find(thisProduct => {
        return thisProduct.product.id === product.id
    })

    return (
        <li className="flex max-w-sm flex-col justify-start items-center p-2 border-2 border-black border-solid" key={product.id}>
            <img className="w-36 overflow-hidden aspect-square" src={`/images/${product.image}`} alt={product.name} />
            <div className="w-full">
                <h2 className="text-xl font-bold">{product.name}</h2>
                <p className="">{product.description?.substring(0, 90)} ...</p>
            </div>
            <div className="w-full flex justify-between items-center">
                <h3 className="text-lg font-bold">{formatPrice(product.price)}</h3>
                {!isIn ? (
                    <button className="p-2 bg-green-500 hover:bg-opacity-50" onClick={() => addProduct(product)}>Add to cart</button>
                ) : (
                    <button className="p-2 bg-red-500 hover:bg-opacity-50" onClick={() => removeProduct(product)}>Remove from cart</button>
                )}
            </div>
        </li>
    )
}

