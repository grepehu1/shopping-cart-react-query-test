import { useEffect, useState } from 'react'
import { useCartContext } from '../contexts/cart'
import { Product } from '../utils/types'
import { formatPrice } from '../utils/utils'

export default function Cart() {
    const { products } = useCartContext()
    const [total, setTotal] = useState(0)

    useEffect(() => {
        let newTotal = 0
        products.forEach(product => {
            const price = product.product.price
            newTotal += price * product.counter
        })
        setTotal(newTotal)
    }, [products])

    return (
        <div className='flex flex-col w-1/3'>
            <table>
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                </tr>
                {products.map(product => (
                    <CartProductItem
                        key={`cart-item-${product.product.id}`}
                        product={product.product}
                        counter={product.counter}
                    />
                ))}
            </table>
            <h2>Total: {formatPrice(total)}</h2>
        </div>
    )
}

function CartProductItem({
    product,
    counter,
}: {
    product: Product,
    counter: number
}) {
    const { changeCounterProduct } = useCartContext()

    return (
        <tr>
            <td>{product.name}</td>
            <td>
                <div className='flex justify-center items-center gap-2'>
                    <button className='flex p-3 w-2 h-2 justify-center items-center bg-gray-400 hover:bg-opacity-50' onClick={() => changeCounterProduct(product, false)}>-</button>
                    <p className='text-md'>{counter}</p>
                    <button className='flex p-3 w-2 h-2 justify-center items-center bg-gray-400 hover:bg-opacity-50' onClick={() => changeCounterProduct(product, true)}>+</button>
                </div>
            </td>
            <td>{formatPrice(product.price)}</td>
        </tr>
    )
}
