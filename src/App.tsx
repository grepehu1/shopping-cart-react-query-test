import { QueryClient, QueryClientProvider } from "@tanstack/react-query"
import { CartProvider } from "./contexts/cart"
import ProductList from "./components/ProductList"
import './index.css'
import Cart from "./components/Cart"

const queryClient = new QueryClient()

function App() {

  return (
    <QueryClientProvider client={queryClient}>
      <CartProvider>
        <div className="flex">
          <ProductList />
          <Cart />
        </div>
      </CartProvider>
    </QueryClientProvider>
  )
}

export default App
