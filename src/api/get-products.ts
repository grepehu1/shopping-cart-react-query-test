import { Product } from "../utils/types";
import products from "./products.json";

export const fetchProducts = async () => {
    const data = await new Promise((resolve) => {
        setTimeout(() => {
            resolve(products);
        }, 2000)
    }) as Product[]
    return data
};