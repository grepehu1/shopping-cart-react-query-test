import { createContext, useCallback, useContext, useEffect, useState } from 'react';
import { Product } from '../utils/types';
import useHasMounted from '../hook/useHasMounted';

const PRODUCTS_IN_CART_KEY = 'ProductsInCart'

interface ProductInCart {
    product: Product,
    counter: number,
}

interface Cart {
    products: ProductInCart[]
    addProduct: (product: Product) => void;
    removeProduct: (product: Product) => void;
    changeCounterProduct: (product: Product, isAdding: boolean) => void;
}

const CartContext = createContext<Cart>({
    products: [],
    addProduct: () => null,
    removeProduct: () => null,
    changeCounterProduct: () => null,
});

export function CartProvider({ children }: React.PropsWithChildren) {
    const hasMounted = useHasMounted()
    const [productsInCart, setProductsInCart] = useState<ProductInCart[]>([])

    function addProduct(product: Product) {
        setProductsInCart(prev => {
            return [...prev, { product, counter: 1 }]
        })
    }

    function removeProduct(product: Product) {
        setProductsInCart(prev => {
            const newProducts = prev.filter(item => {
                if (item.product.id === product.id) return false
                return true
            })
            return newProducts
        })
    }

    function changeCounterProduct(product: Product, isAdding: boolean) {
        setProductsInCart(prev => {
            const newProducts: ProductInCart[] = []

            prev.forEach(item => {
                if (item.product.id !== product.id) {
                    newProducts.push(item)
                    return
                }

                if (item.counter <= 1 && !isAdding) {
                    return
                } else {
                    item.counter += isAdding ? 1 : -1
                }

                newProducts.push(item)
            })
            return newProducts
        })
    }

    function fetchProductsFromBrowser() {
        const rawProducts = localStorage.getItem(PRODUCTS_IN_CART_KEY)
        const newProductsInCart = rawProducts ? JSON.parse(rawProducts) as ProductInCart[] : []
        setProductsInCart(newProductsInCart)
    }

    const addProductsToBrowser = useCallback(() => {
        const rawProducts = JSON.stringify(productsInCart)
        localStorage.setItem(PRODUCTS_IN_CART_KEY, rawProducts)
    }, [productsInCart])

    useEffect(() => {
        fetchProductsFromBrowser()
    }, [])

    useEffect(() => {
        if (hasMounted) {
            addProductsToBrowser()
        }
    }, [addProductsToBrowser, hasMounted])


    const value = {
        products: productsInCart,
        addProduct,
        removeProduct,
        changeCounterProduct,
    }

    return (
        <CartContext.Provider value={value}>
            {children}
        </CartContext.Provider>
    )
}

export function useCartContext() {
    return useContext(CartContext)
}



