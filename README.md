# Test Shopping Cart: React + Tailwind + TS + React-Query

This is a simple solution to solve the given problem: [Build a simple shopping cart using react-query](https://reactpractice.dev/exercise/build-a-simple-shopping-cart-using-react-query/)

I decided to make my own repo from scratch to use tools I like better such as Tailwind, TypeScript and Vite.